const parameter = 'links'
const parameter2 = 'ryandri1314/'
// const url = 'http://path_to_domain' +
//     'path_to_resource' +
//     '?param=' + parameter +
//     '=' + 'param2=' +
//     parameter2;

// const baseUrl = 'https://'
// const path_to_resource = 'facebook.com'
// const url =
// `${baseUrl}/${path_to_resource}?param=${parameter}&param2=${parameter2}`;
    
// console.log(url)

// function method<T>(arg: T): T {
//     return arg;
// }
// let x = method<boolean>(true);
// console.log(x)

class Car {
    private distanceRun: number = 0;
    private color: string;
    
    constructor(private isHybrid: boolean, color: string = 
    'red') {
        this.color = color;
    }
    getGasConsumption(): string {
        return this.isHybrid ? 'Very low' : 'Too high!';
    }
    
    drive(distance: number): void {
        this.distanceRun += distance;
    }
    
    static honk(): string {
        return 'HOOONK!';
    }
    get distance(): number {
        return this.distanceRun;
    }

    set type(type: string) {
        this.type = type;
    }
}

const car = new Car(true, 'red')

console.log(car.getGasConsumption())
car.drive(1000)
car.drive(120)
console.log(car.distance)

console.log(Car.honk())
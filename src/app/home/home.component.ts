import { Component } from '@angular/core';

const sayHello: (name: string) => string = function(name: 
  string): string {
      name = 'Hi'
      return 'Hello, ' + name;
  }
enum StackingIndex {
  None,
  Dropdown,
  Overlay = 2000,
  Modal = 3000
};
const mySelectBoxStacking: StackingIndex = StackingIndex.Overlay
const myCarsName: string = StackingIndex[1]
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent {
  public name = 'Tuyensiuciute';
  public age = 16;
  public vehicles = ['honda', 'toyota', 'yamaha', 'lexus', 'lambogini', 'fernari'];

  public upAge() {
    this.age++;
    if (this.age === 25) {
      alert('Let\'s marry your bae!');
    }
    sessionStorage.setItem('1', JSON.stringify(this.age));
  }
  public print() {
    console.log(mySelectBoxStacking)
    console.log(myCarsName)
    sayHello('Chicken')
  }
  public greetMe(name?: string, greeting: string = 'Hello'): 
  string {
    console.log(greeting)
    return `${greeting}, ${name}`;
  }
}
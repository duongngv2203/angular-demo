#include<iostream>
using namespace std;

int main() {
    int myArray[100] = {};
    int n;
    for (int i = 0; i < 100; ++i) {
        cout << "myArray[" << i << "] = ";
        cin >> n;
        if (n != 0 && n >= 1 && n <= 100) {
            myArray[i] = n;
        }
        if (n == 0) break;
    }

    int countingArray[101] = {};
    for (int i = 0; i < 100; ++i) {
        countingArray[myArray[i]]++; // 1 3 7 2 6 1 9
    }    
    for (int i = 1; i < 101; ++i) {
        if (countingArray[i] != 0) {
            cout << i << " occurs " << countingArray[i];
            if (countingArray[i] > 1) cout << " times\n";
            else cout << " time\n";
        }
        else continue;
    }
    return 0;
}